# Information
Username: leviathan0  
Password: leviathan0  

# Tools
* \*nix tools
  * ls
  * less
  * grep

# Procedure
## Home directory files
The *-a* option can be useful to find hidden files.

```bash
leviathan0@leviathan:~$ ls -al
total 24
drwxr-xr-x  3 root       root       4096 Aug 26  2019 .
drwxr-xr-x 10 root       root       4096 Aug 26  2019 ..
drwxr-x---  2 leviathan1 leviathan0 4096 Aug 26  2019 .backup
-rw-r--r--  1 root       root        220 May 15  2017 .bash_logout
-rw-r--r--  1 root       root       3526 May 15  2017 .bashrc
-rw-r--r--  1 root       root        675 May 15  2017 .profile
leviathan0@leviathan:~$ 
```
Backups are always nice to have! Let's check it out.

## Backup content
```bash
leviathan0@leviathan:~$ ls -al .backup/
total 140
drwxr-x--- 2 leviathan1 leviathan0   4096 Aug 26  2019 .
drwxr-xr-x 3 root       root         4096 Aug 26  2019 ..
-rw-r----- 1 leviathan1 leviathan0 133259 Aug 26  2019 bookmarks.html
leviathan0@leviathan:~$ 
```
Bookmarks usually have good information.

A *less* to this file shows a normal html file. But, since we know one part of the information we want - username is **leviathan1** - then we can search for this string in this file:

```bash
leviathan0@leviathan:~$ grep leviathan1 .backup/bookmarks.html 
<DT><A HREF="http://leviathan.labs.overthewire.org/passwordus.html | This will be fixed later, the password for leviathan1 is rioGegei8m" ADD_DATE="1155384634" LAST_CHARSET="ISO-8859-1" ID="rdf:#$2wIU71">password to leviathan1</A>
```
<br>

>**Houston, we find our flag**

# Flag
```bash
rioGegei8m
```

# Additional notes
http://leviathan.labs.overthewire.org/passwordus.html returns a 404.

```bash
┌─[foo@bar]─[~]
└──╼ $ wget http://leviathan.labs.overthewire.org/passwordus.html
--2021-03-20 18:04:40--  http://leviathan.labs.overthewire.org/passwordus.html
Resolving leviathan.labs.overthewire.org (leviathan.labs.overthewire.org)... 176.9.9.172
Connecting to leviathan.labs.overthewire.org (leviathan.labs.overthewire.org)|176.9.9.172|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2021-03-20 18:04:40 ERROR 404: Not Found.
```
