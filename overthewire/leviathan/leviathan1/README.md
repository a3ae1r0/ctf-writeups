# Information
Username: leviathan1  
Password: rioGegei8m

# Tools
* \*nix tools
  * `file`
  * `strings`
  * `gdb`
  * `ltrace`

# Procedure
## Home directory files

```bash
leviathan1@leviathan:~$ ls -ltra
total 28
-rw-r--r--  1 root       root        675 May 15  2017 .profile
-rw-r--r--  1 root       root       3526 May 15  2017 .bashrc
-rw-r--r--  1 root       root        220 May 15  2017 .bash_logout
drwxr-xr-x 10 root       root       4096 Aug 26  2019 ..
-r-sr-x---  1 leviathan2 leviathan1 7452 Aug 26  2019 check
drwxr-xr-x  2 root       root       4096 Aug 26  2019 .
```
The `check` file looks very interesting.

## Look further into file
A initial `less` to the file shows that is a binary file. So the first thing to do is get more information about this file.

```bash
leviathan1@leviathan:~$ file ./check 
./check: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=c735f6f3a3a94adcad8407cc0fda40496fd765dd, not stripped
```

The `file` command shows that it is a **ELF 32-bit LSB executable**, which is not a bad thing, since it give us more possibilities to explore.

### Run file and get password
The first thing we should do is run the executable and check (no pun intended) the result.

```bash
leviathan1@leviathan:~$ ./check
password: rioGegei8m
Wrong password, Good Bye ...
```

I tried several passwords, but without any success. After this attempts, I tried to explore multiple directories (/etc, /home) and files (.bashrc, .profile), looking for clues, but still without success.

Then I tried to get back and explore the ELF executable.

### Explore file
To explore the file further, I search more information online about ELF files. After some research[^1], I run some \*nix commands against the file, such as `readelf` and `gdb`. But, it was after another webpage[^2] where I found good progress.

The `strace` commands was really useful. The is the output after run it:

```bash
leviathan1@leviathan:~$ ltrace ./check 
__libc_start_main(0x804853b, 1, 0xffffd794, 0x8048610 <unfinished ...>
printf("password: ")                                                         = 10
getchar(1, 0, 0x65766f6c, 0x646f6700password: some_password        
)                                        = 115
getchar(1, 0, 0x65766f6c, 0x646f6700)                                        = 111
getchar(1, 0, 0x65766f6c, 0x646f6700)                                        = 109
strcmp("som", "sex")                                                         = 1
puts("Wrong password, Good Bye ..."Wrong password, Good Bye ...
)                                         = 29
+++ exited (status 0) +++
```
Do you see any peculiar word in this output? (*Hint:* it starts with *s*)

So let's try this word as a password:
```bash
leviathan1@leviathan:~$ ./check 
password: sex
$
```

Seems alright...
```bash
$ whoami
leviathan2
```

And now that we have the right user, let's get the password:
```bash
$ cat /etc/leviathan_pass/leviathan2
ougahZi8Ta
```

>**Houston, we find our flag**

# Flag
```bash
ougahZi8Ta
```

# Resources
[^1]: [Reversing ELF 64-bit LSB executable, x86-64 ,gdb](https://reverseengineering.stackexchange.com/questions/3815/reversing-elf-64-bit-lsb-executable-x86-64-gdb)  
[^2]: [hackme: Deconstructing an ELF File](http://manoharvanga.com/hackme/)
